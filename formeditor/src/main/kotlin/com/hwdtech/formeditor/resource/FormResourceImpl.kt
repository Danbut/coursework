package com.hwdtech.formeditor.resource

import com.hwdtech.formeditor.dto.CreateFormRequest
import com.hwdtech.formeditor.dto.FormResponse
import com.hwdtech.formeditor.dto.UpdateFormRequest
import com.hwdtech.formeditor.resource.FormResourceImpl.Companion.BASE_FORM_URL
import com.hwdtech.formeditor.service.FormManagementService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
@RequestMapping(value = [BASE_FORM_URL])
class FormResourceImpl(private val formManagementService: FormManagementService) : FormResource {

    @GetMapping("/{id}")
    override fun findById(@PathVariable id: Long): ResponseEntity<FormResponse?> {
        val formResponse: FormResponse? = this.formManagementService.findById(id)
        return ResponseEntity.status(HttpStatus.OK).body(formResponse)
    }

    @GetMapping
    override fun findAll(pageable: Pageable): ResponseEntity<Page<FormResponse>> {
        return ResponseEntity.ok(this.formManagementService.findAll(pageable))
    }

    @PostMapping
    override fun save(@RequestBody createFormRequest: CreateFormRequest): ResponseEntity<FormResponse> {
        val formResponse = this.formManagementService.save(createFormRequest)
        return ResponseEntity
            .created(URI.create(BASE_FORM_URL.plus("/${formResponse.id}")))
            .body(formResponse)
    }

    @PutMapping
    override fun update(@RequestBody updateFormRequest: UpdateFormRequest): ResponseEntity<FormResponse> {
        return ResponseEntity.ok(this.formManagementService.update(updateFormRequest))
    }

    @DeleteMapping("/{id}")
    override fun deleteById(@PathVariable id: Long): ResponseEntity<Unit> {
        this.formManagementService.deleteById(id)
        return ResponseEntity.noContent().build()
    }

    companion object {
        const val BASE_FORM_URL: String = "/api/v1/form"
    }
}
