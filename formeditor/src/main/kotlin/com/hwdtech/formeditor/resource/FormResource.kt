package com.hwdtech.formeditor.resource

import com.hwdtech.formeditor.dto.CreateFormRequest
import com.hwdtech.formeditor.dto.FormResponse
import com.hwdtech.formeditor.dto.UpdateFormRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity

interface FormResource {
    fun findById(id: Long): ResponseEntity<FormResponse?>
    fun findAll(pageable: Pageable): ResponseEntity<Page<FormResponse>>
    fun save(addFormRequest: CreateFormRequest): ResponseEntity<FormResponse>
    fun update(updateFormRequest: UpdateFormRequest): ResponseEntity<FormResponse>
    fun deleteById(id: Long): ResponseEntity<Unit>
}
