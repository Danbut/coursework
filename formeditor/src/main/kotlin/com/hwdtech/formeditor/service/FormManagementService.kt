package com.hwdtech.formeditor.service

import com.hwdtech.formeditor.dto.CreateFormRequest
import com.hwdtech.formeditor.dto.FormResponse
import com.hwdtech.formeditor.dto.UpdateFormRequest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface FormManagementService {
    fun findById(id: Long): FormResponse?
    fun findAll(pageable: Pageable): Page<FormResponse>
    fun save(CreateFormRequest: CreateFormRequest): FormResponse
    fun update(updateFormRequest: UpdateFormRequest): FormResponse
    fun deleteById(id: Long)
}
