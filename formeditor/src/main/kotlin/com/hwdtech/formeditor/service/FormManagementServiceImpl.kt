package com.hwdtech.formeditor.service

import com.hwdtech.formeditor.dao.FormDao
import com.hwdtech.formeditor.domain.Form
import com.hwdtech.formeditor.dto.CreateFormRequest
import com.hwdtech.formeditor.dto.FormResponse
import com.hwdtech.formeditor.dto.UpdateFormRequest
import com.hwdtech.formeditor.transformer.CreateFormRequestTransformer
import com.hwdtech.formeditor.transformer.toFormResponse
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class FormManagementServiceImpl(
    private val formDao: FormDao,
    private val createFormRequestTransformer: CreateFormRequestTransformer
) : FormManagementService {

    override fun findById(id: Long): FormResponse? = this.findFormById(id).toFormResponse()

    override fun findAll(pageable: Pageable): Page<FormResponse> = this.formDao.findAll(pageable).map(Form::toFormResponse)

    override fun save(createFormRequest: CreateFormRequest): FormResponse {
        return this.saveOrUpdate(
            createFormRequestTransformer.transform(createFormRequest)
        )
    }

    override fun update(updateFormRequest: UpdateFormRequest): FormResponse {
        val form = this.findFormById(updateFormRequest.id) ?: throw IllegalStateException("${updateFormRequest.id} not found")

        return this.saveOrUpdate(
            form.apply {
                this.name = updateFormRequest.name
            }
        )
    }

    override fun deleteById(id: Long) {
        this.formDao.deleteById(id)
    }

    private fun findFormById(id: Long): Form? = this.formDao.findByIdOrNull(id)

    private fun saveOrUpdate(Form: Form): FormResponse = this.formDao.save(Form).toFormResponse()
}
