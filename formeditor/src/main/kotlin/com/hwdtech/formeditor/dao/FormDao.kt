package com.hwdtech.formeditor.dao

import com.hwdtech.formeditor.domain.Form
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FormDao : JpaRepository<Form, Long>
