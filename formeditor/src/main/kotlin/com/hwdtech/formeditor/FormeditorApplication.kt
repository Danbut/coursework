package com.hwdtech.formeditor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FormeditorApplication

fun main(args: Array<String>) {
	runApplication<FormeditorApplication>(*args)
}
