package com.hwdtech.formeditor.transformer

import com.hwdtech.formeditor.domain.Form
import com.hwdtech.formeditor.dto.FormResponse

fun Form?.toFormResponse(): FormResponse {
    return FormResponse(
        id = this?.id ?: 1L,
        name = "${this?.name}"
    )
}
