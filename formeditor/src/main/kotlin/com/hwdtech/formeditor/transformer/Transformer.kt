package com.hwdtech.formeditor.transformer

interface Transformer <A, B> {
    fun transform(source: A): B
}
