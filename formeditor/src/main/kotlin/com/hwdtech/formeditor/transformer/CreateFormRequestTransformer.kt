package com.hwdtech.formeditor.transformer

import com.hwdtech.formeditor.domain.Form
import com.hwdtech.formeditor.dto.CreateFormRequest
import org.springframework.stereotype.Component

@Component
class CreateFormRequestTransformer : Transformer<CreateFormRequest, Form> {
    override fun transform(source: CreateFormRequest): Form {
        return Form(
            name = source.name
        )
    }
}
