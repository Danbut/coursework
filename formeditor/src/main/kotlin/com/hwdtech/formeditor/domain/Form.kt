package com.hwdtech.formeditor.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator

@Entity
data class Form(
    @Id
    @SequenceGenerator(name = FORM_SEQUENCE, sequenceName = FORM_SEQUENCE, initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = FORM_SEQUENCE)
    val id: Long = 1,
    var name: String = ""
) {

    companion object {
        const val FORM_SEQUENCE: String = "FORM_SEQUENCE"
    }
}
