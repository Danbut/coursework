package com.hwdtech.formeditor.dto

data class CreateFormRequest(val name: String)
