package com.hwdtech.formeditor.dto

data class FormResponse(val id: Long, val name: String)
