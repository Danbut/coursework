package com.hwdtech.formeditor.dto

data class UpdateFormRequest(val id: Long, val name: String)
