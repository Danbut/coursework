# Formeditor

## Get Started

#### Prerequisites

You need to install:

1. [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Docker-compose](https://docs.docker.com/compose/install/)

#### Init docker 

1. Clone the repository
2. Set password of `Postgres` database for `Spring` application in `src/main/resources/application.properties`
3. Set password of `Postgres` database for `Docker` environment in `docker-compose.yml`
4. Start init application with `docker-compose up -d`
5. Check if it's running on `localhost:8080`